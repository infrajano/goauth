FROM golang:1.18 as builder
# Define build env
ENV GOOS linux
ENV CGO_ENABLED 1



# Add a work directory
WORKDIR /app
# Cache and install dependencies
COPY go.mod go.sum ./
RUN go mod download
# Copy app files
COPY . .
# Build app
RUN go build -ldflags="-extldflags=-static" -tags sqlite_omit_load_extension -o app

FROM alpine:3.14 as production
# Add certificates
RUN apk add --no-cache ca-certificates
# Copy built binary from builder
COPY --from=builder app .
# Expose port
EXPOSE 8080
# Exec built binary
CMD ./app